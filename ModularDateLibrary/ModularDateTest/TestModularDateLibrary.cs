﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ModularDateLibrary;

namespace ModularDateTest
{
    [TestClass]
    public class TestModularDateLibrary
    {
        private TestContext testContextInstance;

        public TestContext TestContext
        {
            get { return this.testContextInstance; }
            set { this.testContextInstance = value; }
        }

        [TestMethod]
        public void TestFullDateWithUTCMinusHoursAndMinutes()
        {
            try
            {
                ModularDate dt = new ModularDate("<4,30+2014.12.31.11.59.59.999");
                Assert.IsTrue(dt.Offset == 0-((4*60)+30) &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 && 
                    dt.Millisecond == 999);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestFullDateWithUTCPlusHoursAndMinutes()
        {
            try
            {
                ModularDate dt = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                Assert.IsTrue(dt.Offset == ((4 * 60) + 30) &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 &&
                    dt.Millisecond == 999);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestFullDateWithUTCMinusHoursOnly()
        {
            try
            {
                ModularDate dt = new ModularDate("<4+2014.12.31.11.59.59.999");
                Assert.IsTrue(dt.Offset == 0 - (4 * 60) &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 &&
                    dt.Millisecond == 999);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestFullDateWithUTCPlusHoursOnly()
        {
            try
            {
                ModularDate dt = new ModularDate(">4+2014.12.31.11.59.59.999");
                Assert.IsTrue(dt.Offset == (4 * 60) &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 &&
                    dt.Millisecond == 999);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestFullDateADWithoutUTC()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014.12.31.11.59.59.999");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 &&
                    dt.Millisecond == 999);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestFullDateBCWithoutUTC()
        {
            try
            {
                ModularDate dt = new ModularDate("-2014.12.31.11.59.59.999");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.BC &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 &&
                    dt.Millisecond == 999);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestADWithoutUTCWithoutMilliseconds()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014.12.31.11.59.59");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second == 59 &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestADWithoutUTCWithoutSeconds()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014.12.31.11.59");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute == 59 && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestADWithoutUTCWithoutMinutes()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014.12.31.11");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour == 11 && dt.Minute.HasValue == false && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestADWithoutUTCWithoutHours()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014.12.31");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day == 31 &&
                    dt.Hour.HasValue == false && dt.Minute.HasValue == false && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestADWithoutUTCWithoutDays()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014.12");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month == 12 && dt.Day.HasValue == false &&
                    dt.Hour.HasValue == false && dt.Minute.HasValue == false && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestADWithoutUTCWithoutMonths()
        {
            try
            {
                ModularDate dt = new ModularDate("+2014");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 2014 && dt.Month.HasValue == false && dt.Day.HasValue == false &&
                    dt.Hour.HasValue == false && dt.Minute.HasValue == false && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestReallyBigAD()
        {
            try
            {
                ModularDate dt = new ModularDate("+9876543210");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.AD &&
                    dt.Year == 9876543210 && dt.Month.HasValue == false && dt.Day.HasValue == false &&
                    dt.Hour.HasValue == false && dt.Minute.HasValue == false && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestReallyBigBC()
        {
            try
            {
                ModularDate dt = new ModularDate("-9876543210");
                Assert.IsTrue(dt.Offset.HasValue == false &&
                    dt.Era == Eras.BC &&
                    dt.Year == 9876543210 && dt.Month.HasValue == false && dt.Day.HasValue == false &&
                    dt.Hour.HasValue == false && dt.Minute.HasValue == false && dt.Second.HasValue == false &&
                    dt.Millisecond.HasValue == false);
            }
            catch
            {
                Assert.Fail("Full date was not parsed correctly.");
            }
        }

        [TestMethod]
        public void TestEquals_ADFullDateWithUTC()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                Assert.IsTrue(dtFirst == dtSecond);
            }
            catch
            {
                Assert.Fail("First date does not equal second date");
            }
        }

        [TestMethod]
        public void TestEquals_ADFullDateWithoutUTC()
        {
            try
            {
                ModularDate dtFirst = new ModularDate("+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate("+2014.12.31.11.59.59.999");
                Assert.IsTrue(dtFirst == dtSecond);
            }
            catch
            {
                Assert.Fail("First date does not equal second date");
            }
        }

        [TestMethod]
        public void TestEquals_ADDateOnlyWithoutUTC()
        {
            try
            {
                ModularDate dtFirst = new ModularDate("+2014.12.31");
                ModularDate dtSecond = new ModularDate("+2014.12.31");
                Assert.IsTrue(dtFirst == dtSecond);
            }
            catch
            {
                Assert.Fail("First date does not equal second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentOffset()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate("<4,30+2014.12.31.11.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentOffsetMinutes()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentEra()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30-2014.12.31.11.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentYear()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2015.12.31.11.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentMonth()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.10.31.11.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }


        [TestMethod]
        public void TestNotEquals_DifferentDay()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.30.11.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }


        [TestMethod]
        public void TestNotEquals_DifferentHour()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.31.12.59.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentMinute()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.31.11.58.59.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentSecond()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.31.11.59.58.999");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

        [TestMethod]
        public void TestNotEquals_DifferentMillisecond()
        {
            try
            {
                ModularDate dtFirst = new ModularDate(">4,30+2014.12.31.11.59.59.999");
                ModularDate dtSecond = new ModularDate(">4,30+2014.12.31.11.59.59.998");
                Assert.IsTrue(dtFirst != dtSecond);
            }
            catch
            {
                Assert.Fail("First date equals second date");
            }
        }

    }
}
