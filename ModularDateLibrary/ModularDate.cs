﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ModularDateLibrary
{
    public enum Eras : byte
    {
        BC = 0,
        AD = 1
    }

    public class ModularDate : IComparable
    {
        #region Static Output Options
        /// <summary>
        /// Set this to true if you wish to use CE/BCE notation, otherwise AD/BC notation will be used.
        /// </summary>
        public static bool UseCE = false;
        /// <summary>
        /// Set this to false if you do not want the AD/BC or CE/BCE notations included in the output strings.
        /// </summary>
        public static bool IncludeEra = true;
        #endregion

        #region Static Operator Overloads
        public static bool operator ==(ModularDate a, ModularDate b)
        {
            return a.ValueEquals (b);
        }

        public static bool operator !=(ModularDate a, ModularDate b)
        {
            return !(a.ValueEquals (b));
        }
        #endregion

        #region Static Helper Methods
        private static decimal CalculateTicks(ModularDate date)
        {
            // units of time are sequential, we can essentially shift the units into higher orders for comparison
            // 2014.12.31.11.59.59.999 == 20,141,231,115,959,999
            //                            10,100,100,100,100,100
            //                            YY YYM MDD HHM MSS MMM 

            decimal ticks = date.Year * 10000000000000000;
            if (date.Month.HasValue) ticks += date.Month.Value * 1000000000000;
            if (date.Day.HasValue) ticks += date.Day.Value * 10000000000;
            if (date.Hour.HasValue) ticks += date.Hour.Value * 100000000;
            if (date.Minute.HasValue) ticks += date.Minute.Value * 100000;
            if (date.Second.HasValue) ticks += date.Second.Value * 10000;
            if (date.Millisecond.HasValue) ticks += date.Millisecond.Value;

            if (date.Era == Eras.BC)
            {
                return 0 - ticks;
            }
            else
            {
                return ticks;
            }
        }
        #endregion

        #region Regular Expression Pattern
        /// <summary>
        /// The regular expression matching string to determine if incoming date is in the correct format
        /// </summary>
        private const string PATTERN = @"(([<>][0-9]{1,2})(\,[0-9]{1,2})?)?([\+-][0-9]+)(?<![\+-]0)(\.[0-9]{2}(\.[0-9]{2}(\.[0-9]{2}(\.[0-9]{2}(\.[0-9]{2}(\.[0-9]{3})?)?)?)?)?)?";
        #endregion

        #region Properties
        /// <summary>
        /// Gets or sets the era to which this date belongs. The era is a required value.
        /// </summary>
        public Eras Era;
        /// <summary>
        /// Gets or sets the year for this date.  Can be any 64-bit integer value so that extreme past and future can be represented (although not validated).  The year is a required value.
        /// </summary>
        public long Year;
        /// <summary>
        /// Gets or sets the month for this date (1-12).  
        /// </summary>
        public Nullable<byte> Month;
        /// <summary>
        /// Gets or sets the day for this date (0-31).
        /// </summary>
        public Nullable<byte> Day;
        /// <summary>
        /// Gets or sets the hour for this date (0-23).
        /// </summary>
        public Nullable<byte> Hour;
        /// <summary>
        /// Gets or sets the minute for this date (0-59).
        /// </summary>
        public Nullable<byte> Minute;
        /// <summary>
        /// Gets or sets the second for this date (0-59).
        /// </summary>
        public Nullable<byte> Second;
        /// <summary>
        /// Gets or sets the millisecond for this date (0-999).
        /// </summary>
        public Nullable<short> Millisecond;
        /// <summary>
        /// Gets or sets the number of minutes that this date is offset from UTC.
        /// </summary>
        public Nullable<int> Offset;
        #endregion

        public ModularDate()
        {
        }

        #region Constructor: from string date
        public ModularDate(string date)
        {
            // full date pattern:  >14,30+2014.12.31.23.59.59.999
            // parts:
            //  - offset: (optional) starts with > or <.  > indicates UTC+trailing number, < indicates UTC-trailing number, (0-14)
            //  - era:  (mandatory) either a + or a -, + indicates AD, - indicates BC
            //  - year: (mandatory) a number other than zero, can be up to 63 bits longs
            //  - month: (optional) a two digit month (01-12)
            //  - day: (optional) a two digit day (01-31, depending on month)
            //  - hour: (optional) a two digit hour (00-23)
            //  - minute: (optional) a two digit minute (00-59)
            //  - second: (optional) a two digit second (00-59)
            //  - millisecond: (optional) a three digit millisend (000-999)

            if (System.Text.RegularExpressions.Regex.IsMatch(date, PATTERN))
            {
                if (date.StartsWith("<") || date.StartsWith(">"))
                {
                    // break out offset
                    string offsetIndicator = date.Substring(0, 1);
                    string offsetFragment = date.Substring(1, date.IndexOfAny(new char[] { '+', '-' }) - 1);

                    string[] offsetElements = offsetFragment.Split(',');

                    if (offsetElements.Length >= 1)
                    {
                        short value = 0;
                        if (short.TryParse(offsetElements[0], out value))
                        {
                            this.Offset = value * 60;  // multiple hours value by 60 to convert to minutes
                        }
                        else
                        {
                            throw new Exception("Unable to parse UTC offset value");
                        }
                    }

                    if (offsetElements.Length >= 2)
                    {
                        short value = 0;
                        if (short.TryParse(offsetElements[1], out value))
                        {
                            this.Offset += value;  // add minutes to value
                        }
                    }

                    if (offsetIndicator.Equals("<"))
                    {
                        // adjust for UTC - (value)
                        this.Offset = 0 - this.Offset;
                    }

                    // trim out offset from date string
                    date = date.Substring(offsetFragment.Length + 1);
                }

                // parse date
                // first character in string must either be + or - to indicate era
                if (date.StartsWith("+"))
                {
                    this.Era = Eras.AD;
                }
                else if (date.StartsWith("-"))
                {
                    this.Era = Eras.BC;
                }
                else
                {
                    throw new Exception("Unrecognized era indicator");
                }

                // trim out era indicator
                date = date.Substring(1);

                // remaining segments are all separated by a "."
                string[] dateSegments = date.Split('.');
                for (int i = 0; i < dateSegments.Length; i++)
                {
                    if (i == 0) // special case, since years can be huge
                    {
                        long value = 0;
                        if (long.TryParse(dateSegments[i], out value))
                        {
                            this.Year = value;
                        }
                        else
                        {
                            throw new Exception("Unrecognized year value");
                        }
                    }
                    else if (i == 6)  // special case, milliseconds are shorts
                    {
                        short value = 0;
                        if (short.TryParse(dateSegments[i], out value))
                        {
                            if (value >= 0 && value <= 999)
                            {
                                this.Millisecond = value;
                            }
                            else
                            {
                                throw new Exception("Invalid millisecond value");
                            }
                        }
                        else
                        {
                            throw new Exception("Unrecognized millisecond value");
                        }
                    }
                    else
                    {
                        byte value = 0;
                        if (byte.TryParse(dateSegments[i], out value))
                        {
                            switch (i)
                            {
                                case 1: // month
                                    if (value >= 1 && value <= 12)
                                    {
                                        this.Month = value;
                                    }
                                    else
                                    {
                                        throw new Exception("Invalid month value");
                                    }
                                    break;

                                case 2: // day
                                    if (value >= 1 && value <= 31)
                                    {
                                        this.Day = value;  // actual day value will be validated later
                                    }
                                    else
                                    {
                                        throw new Exception("Invalid day value");
                                    }
                                    break;

                                case 3: // hour
                                    if (value >= 0 && value <= 23)
                                    {
                                        this.Hour = value;
                                    }
                                    else
                                    {
                                        throw new Exception("Invalid hour value");
                                    }
                                    break;

                                case 4: // minute
                                    if (value >= 0 && value <= 59)
                                    {
                                        this.Minute = value;
                                    }
                                    else
                                    {
                                        throw new Exception("Invalid minute value");
                                    }
                                    break;

                                case 5: // second
                                    if (value >= 0 && value <= 59)
                                    {
                                        this.Second = value;
                                    }
                                    else
                                    {
                                        throw new Exception("Invalid second value");
                                    }
                                    break;
                            }
                        }
                        else
                        {
                            throw new Exception("Invalid date value");
                        }
                    }
                }

                // attempt to validate modern dates
                if (this.Year > 0 && this.Year <= 9999 && this.Month.HasValue && this.Day.HasValue)
                {
                    try
                    {
                        DateTime dt = new DateTime((int)this.Year, (int)this.Month.Value, (int)this.Day.Value);
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                }
            }
        }
        #endregion

        #region ToDateTime()
        public DateTime ToDateTime()
        {
            if (this.Year > 0 && this.Year <= 9999)
            {
                if (this.Month.HasValue && this.Day.HasValue && this.Hour.HasValue && this.Minute.HasValue && this.Second.HasValue && this.Millisecond.HasValue)
                {
                    return new DateTime ((int)this.Year, this.Month.Value, this.Day.Value, this.Hour.Value, this.Minute.Value, this.Second.Value, this.Millisecond.Value, this.Offset.HasValue ? DateTimeKind.Utc : DateTimeKind.Unspecified);
                }
                else if (this.Month.HasValue && this.Day.HasValue && this.Hour.HasValue && this.Minute.HasValue && this.Second.HasValue)
                {
                    return new DateTime ((int)this.Year, this.Month.Value, this.Day.Value, this.Hour.Value, this.Minute.Value, this.Second.Value, this.Offset.HasValue ? DateTimeKind.Utc : DateTimeKind.Unspecified);
                }
                else if (this.Month.HasValue && this.Day.HasValue && this.Hour.HasValue && this.Minute.HasValue)
                {
                    return new DateTime ((int)this.Year, this.Month.Value, this.Day.Value, this.Hour.Value, this.Minute.Value, 0, this.Offset.HasValue ? DateTimeKind.Utc : DateTimeKind.Unspecified);
                }
                else if (this.Month.HasValue && this.Day.HasValue && this.Hour.HasValue)
                {
                    return new DateTime ((int)this.Year, this.Month.Value, this.Day.Value, this.Hour.Value, 0, 0, this.Offset.HasValue ? DateTimeKind.Utc : DateTimeKind.Unspecified);
                }
                else if (this.Month.HasValue && this.Day.HasValue)
                {
                    return new DateTime ((int)this.Year, this.Month.Value, this.Day.Value);
                }
                else if (this.Month.HasValue)
                {
                    return new DateTime ((int)this.Year, this.Month.Value, 1);
                }
                else
                {
                    return new DateTime ((int)this.Year, 1, 1);
                }
            }
            else
            {
                throw new Exception ("Year is outside range of DateTime data type");
            }
        }

        public DateTimeOffset ToDateTimeOffset()
        {
            if (this.Offset.HasValue)
            {
                DateTime tempDate = this.ToDateTime ();
                return new DateTimeOffset (tempDate, new TimeSpan (this.Offset.Value * 60000));
            }
            else
            {
                throw new Exception("Date does not contain a timezone offset");
            }
        }
        #endregion

        #region ToString()
        public override string ToString ()
        {
            try
            {
                System.Text.StringBuilder output = new StringBuilder();
                if (ModularDate.IncludeEra)
                {
                    // CE/BCE and AD all precede the printed date, BC follows after the printed date
                    if (ModularDate.UseCE || this.Era == Eras.AD)
                    {
                        if (this.Era == Eras.AD && ModularDate.UseCE == false) 
                            output.Append("AD ");
                        else if (this.Era == Eras.AD && ModularDate.UseCE == true)
                            output.Append("CE ");
                        else if (this.Era == Eras.BC && ModularDate.UseCE == true)
                            output.Append("BCE ");
                    }
                }
                output.Append(this.Year.ToString("#0"));
                if (this.Month.HasValue)
                {
                    output.Append("-" + this.Month.Value.ToString("00"));
                }
                if (this.Day.HasValue)
                {
                    output.Append("-" + this.Day.Value.ToString("00"));
                }
                if (this.Hour.HasValue)
                {
                    output.Append(" " + this.Hour.Value.ToString("00"));
                }
                if (this.Minute.HasValue)
                {
                    output.Append(":" + this.Minute.Value.ToString("00"));
                }
                if (this.Second.HasValue)
                {
                    output.Append(":" + this.Second.Value.ToString("00"));
                }
                if (this.Millisecond.HasValue)
                {
                    output.Append("." + this.Millisecond.Value.ToString());
                }

                if (ModularDate.IncludeEra && ModularDate.UseCE == false && this.Era == Eras.BC)
                {
                    output.Append (" BC");
                }

                return output.ToString();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Value and Reference Equals
        public bool ValueEquals(ModularDate target)
        {
            if (this.Offset.HasValue != target.Offset.HasValue)
            {
                return false;
            }
            else if (this.Offset.HasValue && target.Offset.HasValue)
            {
                if (!(this.Offset.Value == target.Offset.Value))
                    return false;
            }

            if (!(this.Year == target.Year))
                return false;

            if (this.Month.HasValue != target.Month.HasValue)
            {
                return false;
            }
            else if (this.Month.HasValue && target.Month.HasValue)
            {
                if (this.Month.Value != target.Month.Value)
                    return false;
            }

            if (this.Day.HasValue != target.Day.HasValue)
            {
                return false;
            }
            else if (this.Day.HasValue && target.Day.HasValue)
            {
                if (this.Day.Value != target.Day.Value)
                    return false;
            }

            if (this.Hour.HasValue != target.Hour.HasValue)
            {
                return false;
            }
            else if (this.Hour.HasValue && target.Hour.HasValue)
            {
                if (this.Hour.Value != target.Hour.Value)
                    return false;
            }

            if (this.Minute.HasValue != target.Minute.HasValue)
            {
                return false;
            }
            else if (this.Minute.HasValue && target.Minute.HasValue)
            {
                if (this.Minute.Value != target.Minute.Value)
                    return false;
            }

            if (this.Second.HasValue != target.Second.HasValue)
            {
                return false;
            }
            else if (this.Second.HasValue && target.Second.HasValue)
            {
                if (this.Second.Value != target.Second.Value)
                    return false;
            }

            if (this.Millisecond.HasValue != target.Millisecond.HasValue)
            {
                return false;
            }
            else if (this.Millisecond.HasValue && target.Millisecond.HasValue)
            {
                if (this.Millisecond.Value != target.Millisecond.Value)
                    return false;
            }

            if (!(this.Era == target.Era))
                return false;

            return true;
        }

        public bool ReferenceEquals(ModularDate target)
        {
            return this.Equals (target);
        }
        #endregion

        #region IComparable implementation
        int IComparable.CompareTo (object obj)
        {
            ModularDate target = obj as ModularDate;

            decimal sourceTicks = ModularDate.CalculateTicks (this);
            decimal targetTicks = ModularDate.CalculateTicks (target);

            return sourceTicks.CompareTo (targetTicks);
        }
        #endregion
    }

}
