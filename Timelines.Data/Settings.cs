﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timelines.Data
{
    public class Settings
    {
        public static string HostName { get; set; }
        public static int Port { get; set; }
        public static string DatabaseName { get; set; }
        public static string UserName { get; set; }
        public static System.Security.SecureString Password { get; set; }
        public static int PoolSize { get; set; }
    }
}
