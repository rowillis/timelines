﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timelines.Data
{
    public class Episode
    {
        public Episode()
        {
            this.TimeEntries = new List<TimeEntry>();
        }

        public string Title { get; set; }
        public string Summary { get; set; }
 
        // TODO: determine a way to capture sourcing for the event
        // - perhaps as a graph relationship
        // - perhaps as an implemented interface

        public List<TimeEntry> TimeEntries { get; set; }
    }
}
