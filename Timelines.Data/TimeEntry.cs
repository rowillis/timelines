﻿using ModularDateLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timelines.Data
{
    public enum TimeEntryTypes : byte
    {
        Point = 0,
        Start = 1,
        End = 2
    }

    public class TimeEntry
    {
        public TimeEntryTypes Type { get; set; }
        public ModularDate Date { get; set; }
    }
}
