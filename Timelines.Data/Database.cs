﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timelines.Data
{
    public class Database
    {
        private Orient.Client.ODatabase database = null;

        public void Connect()
        {
            Orient.Client.OClient.CreateDatabasePool(
                Settings.HostName, Settings.Port, Settings.DatabaseName, Orient.Client.ODatabaseType.Graph,
                Settings.UserName, Settings.Password.ToString(), Settings.PoolSize, "Timelines");    

            using (this.database = new Orient.Client.ODatabase("Timelines"))
            {

            }
        }
    }
}
