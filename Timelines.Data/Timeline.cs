﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Timelines.Data
{
    public class Timeline
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public List<Episode> Episodes { get; set; }

        // TODO: Need to define channels and a way to tie episodes to specific channels based on zoom level
    }
}
